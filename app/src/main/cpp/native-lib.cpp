#include <jni.h>
#include <string>
#include "First.h"
#include <syslog.h>
#include <iostream>
#include <fstream>
#include <Calibration.h>
#include <opencv2/opencv.hpp>

using namespace cv;
using namespace std;
Mat & refToParam(Mat & param)
{
    return param;
};

extern "C" {

    Mat *globalFrame;

    JNIEXPORT void JNICALL
    Java_com_example_bartlomiej_jnicameracalibration_MainActivity_getFrame(JNIEnv *env, jclass type,
    jlong nativeObjAdd) {
        globalFrame =  (Mat*)nativeObjAdd;
//        cvtColor(*globalFrame, *globalFrame, COLOR_RGBA2GRAY);
//        GaussianBlur((*globalFrame), (*globalFrame), Size(7, 7), 1.5, 1.5);
//        Canny((*globalFrame), (*globalFrame), 0, 30, 3);
    }


    JNIEXPORT jstring JNICALL
    Java_com_example_bartlomiej_jnicameracalibration_Calibration_readFromFile(JNIEnv *env,
    jobject instance,
    jstring path_) {
        const char *path = env->GetStringUTFChars(path_, 0);
        string line;
        string wholeText;
        ifstream myfile(path);
        if(myfile.is_open())
        {
            while(getline(myfile,line))
            {
                wholeText += line;
            }
            myfile.close();
        }

        env->ReleaseStringUTFChars(path_, path);

        return env->NewStringUTF(wholeText.c_str());
    }


    JNIEXPORT void JNICALL
    Java_com_example_bartlomiej_jnicameracalibration_Calibration_startCalibration(JNIEnv *env,
                                                                                  jobject instance,
                                                                                  jlong nativeObjAdd) {
        Mat& calibMat =  *(Mat*)nativeObjAdd;

        Calibration calibration;
        calibration.globalFramePointer = globalFrame;
        calibration.main(calibMat);

    }

    JNIEXPORT void JNICALL
    Java_com_example_bartlomiej_jnicameracalibration_Calibration_saveToFile(JNIEnv *env,
                                                                            jobject instance,
                                                                            jstring path_) {
        const char *path = env->GetStringUTFChars(path_, 0);

        ofstream myfile;
        myfile.open(path);

        if(myfile.is_open()){
            myfile<<"Czy się wczytało";
            myfile.close();
        }
        env->ReleaseStringUTFChars(path_, path);
    }

    JNIEXPORT jstring JNICALL
    Java_com_example_bartlomiej_jnicameracalibration_Calibration_getMessageFromJNI(JNIEnv *env,
                                                                                   jclass type) {
        First first("Poszło1");
        return env->NewStringUTF(first.pojemnikNa.c_str());
    }


    JNIEXPORT jstring JNICALL
    Java_com_example_bartlomiej_jnicameracalibration_MainActivity_stringFromJNI(
            JNIEnv *env,
            jobject /* this */) {
        std::string hello = "Poszło2";
        return env->NewStringUTF(hello.c_str());
    }


    JNIEXPORT jobject JNICALL
    Java_com_example_bartlomiej_jnicameracalibration_Calibration_getTestForClass(JNIEnv *env,
                                                                                 jobject instance) {

        jclass cls = env->FindClass("com/example/bartlomiej/jnicameracalibration/TestForClass");
        jmethodID constructor = env->GetMethodID(cls, "<init>", "()V");
        return env->NewObject(cls, constructor);
    }

}