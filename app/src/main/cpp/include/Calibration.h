//
// Created by bartlomiej on 04.12.17.
//

#ifndef JNICAMERACALIBRATION_CALIBRATION_H
#define JNICAMERACALIBRATION_CALIBRATION_H

#include <iostream>
#include <sstream>
#include <string>
#include <ctime>
#include <cstdio>

#include <opencv2/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <Settings.h>

class Calibration {
public:
    Mat* globalFramePointer;
    int main(Mat& calibMat);
};


#endif //JNICAMERACALIBRATION_CALIBRATION_H
