//
// Created by bartlomiej on 30.11.17.
//

#ifndef JNICAMERACALIBRATION_NATIVECLASS_H
#define JNICAMERACALIBRATION_NATIVECLASS_H


#include <string>

class First {
public:
    First(const std::string &pojemnikNaDupe);

public:
    std::string pojemnikNa;
    std::string jakasFunkcja();

};

#endif //JNICAMERACALIBRATION_NATIVECLASS_H
