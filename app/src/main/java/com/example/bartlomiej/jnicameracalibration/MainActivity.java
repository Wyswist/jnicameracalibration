package com.example.bartlomiej.jnicameracalibration;

import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2{
    public Mat mRGBa, mCalibMat;
    JavaCameraView javaCameraView;

    private static final String TAG = "MainActivity";

    boolean onceHappend = false;
    Calibration calibration;

    Handler customHandler = new Handler();


    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
        System.loadLibrary("opencv_java3");
    }

    //On inicjuje opencv
    BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch(status){
                case BaseLoaderCallback.SUCCESS:
                    javaCameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        javaCameraView =  (JavaCameraView)findViewById(R.id.java_camera_view);
        javaCameraView.setVisibility(View.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);

        // Example of a call to a native method

        String path = Environment.getExternalStorageDirectory().getPath() + "/Ass.txt";
        Log.d("Sciezka", path);
//        calibration = new Calibration();
//        calibration.startCalibration();
//        TestForClass testForClass = calibration.getTestForClass();
//        calibration.saveToFile(path);
//        String text = calibration.readFromFile(path);
//        TextView tv = (TextView) findViewById(R.id.sample_text);
//        tv.setText(text.toString());
//        Log.d("DUPA", testForClass.toString());
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    public String getFileText(String path){
        StringBuilder text = new StringBuilder();
        try {
            FileReader fileReader = new FileReader(path);
            BufferedReader br = new BufferedReader(fileReader);
            String line;
             while ((line = br.readLine()) != null){
                 text.append(line);
                 text.append('\n');
             }
             br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text.toString();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(javaCameraView!=null)
            javaCameraView.disableView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(javaCameraView!=null)
            javaCameraView.disableView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(OpenCVLoader.initDebug()){
            Log.d("onResume","Aplikacja w onResume mode");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        else{
            Log.d("onResume","onResume nie powiodło się");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_3_0, this, mLoaderCallback);
        }
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        mRGBa = new Mat(height, width, CvType.CV_8UC4);
        mCalibMat = new Mat(height, width, CvType.CV_8UC4);
    }

    @Override
    public void onCameraViewStopped() {
        mRGBa.release();
    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRGBa = inputFrame.rgba();
        getFrame(mRGBa.getNativeObjAddr());
        if(!onceHappend){
            onceHappend = true;
            calibration = new Calibration(mCalibMat);
            new Thread(calibration).start();
        }
//        if(mCalibMat.getNativeObjAddr()!=mRGBa.getNativeObjAddr())
            return mCalibMat;
//        else
//            return new Mat();
    }

    public static native void getFrame(long nativeObjAdd);
}
