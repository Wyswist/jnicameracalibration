package com.example.bartlomiej.jnicameracalibration;

import org.opencv.core.Mat;

/**
 * Created by bartlomiej on 30.11.17.
 */

public class Calibration implements Runnable {
    Mat mCalibMat;
    public Calibration(Mat param){
        mCalibMat = param;
    }
    @Override
    public void run() {
        startCalibration(mCalibMat.getNativeObjAddr());
    }
    public native void startCalibration(long nativeObjAdd);


    //    public native static String getMessageFromJNI();
//    public native TestForClass getTestForClass();
//    public native void saveToFile(String path);
//    public native String readFromFile(String path);
}
